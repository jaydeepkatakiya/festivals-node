import { responseHandler } from '../../config/ResponseHandler.js'
import { Festivals } from '../models/Festivals.js'
import _ from 'lodash'
import fs from 'fs'

export const create = async (req, res, next) => {
  try {
    if (req.file.filename) {
      req.body.image = req.file.filename
    }
    let post = new Festivals(req.body)
    post.save((err, post) => {
      if (err) {
        return res.send(responseHandler(null, true, err))
      }
      return res.send(responseHandler(post))
    })
  } catch (error) {
    next(error)
  }
}

export const getSingle = async (req, res, next) => {
  try {
    if (!req.params.id) {
      return errorHandle(404, 'Festival not found!', res)
    }
    const singleFest = await Festivals.find({
      _id: req.params.id,
      isDeleted: false,
    })
    res.send(responseHandler(singleFest))
  } catch (error) {
    next(error)
  }
}

export const getAll = async (req, res, next) => {
  try {
    let query = req.query
    let mongoQuery = { isDeleted: false }
    let skip = 0
    let limit = 0
    for (let key in query) {
      if (key == 'start') {
        skip = parseInt(query[key])
      } else if (key == 'end') {
        limit = parseInt(query[key]) - skip + 1
      }
    }

    const postData = await Festivals.find(mongoQuery)
      .sort({ date: 1 })
      .skip(skip)
      .limit(limit)
    res.send(responseHandler(postData, postData.length))
  } catch (error) {
    next(error)
  }
}

export const update = async (req, res, next) => {
  try {
    const id = req.params.id
    if (!id) {
      id = req.body._id
    }
    if (req.body._id) {
      delete req.body._id
    }

    const things = await Festivals.findOne({ _id: id })

    if (!things) {
      return res.send(new ErrorHandler(404, 'Festival not found!'))
    }

    let updated = _.assign(things, req.body)

    if (!updated) {
      return res.send(new ErrorHandler(404, 'Festival not found!'))
    }

    updated.save()

    return res.send(responseHandler(updated))
  } catch (error) {
    next(error)
  }
}

export const Search = async (req, res, next) => {
  try {
    let query = req.body.search
    if (!query) {
      return res.send(responseHandler(null, true, 'Send search query first!'))
    }
    let userData = await Festivals.find({
      $or: [
        { title: { $regex: new RegExp(query, 'i') } },
        { country: { $regex: new RegExp(query, 'i') } },
      ],
    }).select('_id title country date')
    return res.send(responseHandler({ userData }))
  } catch (error) {
    next(error)
  }
}
